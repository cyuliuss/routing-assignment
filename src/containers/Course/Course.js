import React, { Component } from 'react';

class Course extends Component {
    state = {
        loadedCourse: null,
        courseTitle: null
    }
    componentDidMount() {
        this.loadData();
    }

    componentDidUpdate() {
        this.loadData();
    }

    loadData() {
        if (!this.state.loadedCourse ||
            (this.state.loadedCourse && this.state.loadedCourse !== this.props.match.params.id)) {
            const query = new URLSearchParams(this.props.location.search);
            let title = null;
            for (let param of query.entries()) {
                title = param;
            }

            this.setState({ loadedCourse: this.props.match.params.id, courseTitle: title })
        }
    }
    render() {
        let course = null;
        if (this.props.match.params.id) {
            course = <p style={{ textAlign: 'center' }}>Loading...!</p>;
        }

        if (this.state.loadedCourse) {
            course = (
                <div className="Course">
                    <h1>{this.state.loadedCourse} . {this.state.courseTitle} </h1>
                </div>

            );
        }
        return course;
    }
}

export default Course;