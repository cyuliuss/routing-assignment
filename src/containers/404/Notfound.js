import React, { Component } from 'react';

class Notfound extends Component {
    componentDidMount() {
        console.log(this.props);
    }

    render () {
        return (
            <div>
                <h1>Not Found</h1>
                <p>Page Not Found</p>
            </div>
        );
    }
}

export default Notfound;