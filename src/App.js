import React, { Component } from 'react';

import Courses from './containers/Courses/Courses';
import Course from './containers/Course/Course';

import Users from './containers/Users/Users';
import Notfound from './containers/404/Notfound';
import './App.css';

import { BrowserRouter, Route, NavLink, Switch, Redirect } from 'react-router-dom';

class App extends Component {
  render() {
    return (

      <BrowserRouter bsn>
        <div className="App">
          <header>
            <nav>
              <ul>
                <li><NavLink  to="/users" >User</NavLink></li>
                <li><NavLink  to="/courses" >Courses </NavLink></li>
              </ul>
            </nav>
          </header>
          <Switch>
            <Route path="/users" exact component={Users} />
            <Route path="/courses"   component={Courses} />
            <Route path="/" exact  component={Courses} />
            <Redirect from="/all-courses" to="/courses" />
            <Route component={Notfound} /> 
          </Switch>
          
        </div>
      </BrowserRouter>
       
    );
  }
}

export default App;
